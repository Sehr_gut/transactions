public class Account
{
    private long money;
    private String accNumber;

    public Account(){
        this.money = (long)(Math.random() * 100000);
        this.accNumber = "Номер: " + (long)(Math.random() * 10000000);
    }

    public Account(long money, String accNumber){
        this.money = money;
        this.accNumber = accNumber;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }


}
