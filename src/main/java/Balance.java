import java.util.HashMap;
import java.util.Map;

public class Balance implements  Runnable {

  private Bank bank;
  private int i;
  private Map<Integer, Account> hashMap;

  public Balance(Bank bank, int i, Map<Integer, Account> hashMap){
    this.bank = bank;
    this.i = i;
    this.hashMap = hashMap;
  }

  @Override
  public void run() {
    System.out.println(bank.getBalance(hashMap.get(i).getAccNumber()));
  }
}
