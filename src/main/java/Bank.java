import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Bank
{
    private Map<Integer, Account> accounts = new HashMap<>();
    private Map<String, Account> lockedAccounts = Collections.synchronizedMap(new HashMap<>());

    private final Random random = new Random();

    private final long amountForChecking = 50000;

    public Bank (){
        for (int i = 1 ; i <= 100; i++){
            Account account = new Account();
            accounts.put(i, account);
        }
    }

    public Bank (Map<Integer, Account> accounts){
        this.accounts = accounts;
    }

    public synchronized boolean isFraud() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

        return random.nextBoolean();
    }


    public void transfer(int fromAccountNum, int toAccountNum, long amount) {

        if (fromAccountNum == toAccountNum){
          System.out.println("Перевод невозможен, счета одинаковые!");
          return;
        }

        if (!accounts.containsKey(fromAccountNum) || !accounts.containsKey(toAccountNum)) {
          System.out.println("Одного или обоих акканутов не существует!");
          return;
        }

        Account accFrom = accounts.get(fromAccountNum);
        Account accTo = accounts.get(toAccountNum);

        if (lockedAccounts.containsKey(accFrom.getAccNumber()) || lockedAccounts.containsKey(accTo.getAccNumber())){
          System.out.println("Один из счетов или оба заблокированы!");
          return;
        }

        Account lowSync = accFrom.getAccNumber().compareTo(accTo.getAccNumber()) >= 1 ? accFrom : accTo;
        Account highSync = accFrom.getAccNumber().compareTo(accTo.getAccNumber()) >= 1 ? accTo : accFrom;

        synchronized (lowSync) {
          synchronized (highSync) {
            doTransaction(amount, accFrom, accTo);
          }
        }

    }

    private void doTransaction(long amount, Account accFrom, Account accTo){

      if (amount > amountForChecking && isFraud()) {
        System.out.println(
            "Операция между счетами: " + accFrom.getAccNumber()
                + " и " + accTo.getAccNumber() + " заблокирована");

        lockAccount(accFrom, accTo);
        return;
      }

      if (amount > accFrom.getMoney()) {
        System.out.println("Недостаточно средств");
        return;
      }

          accFrom.setMoney(accFrom.getMoney() - amount);
          accTo.setMoney(accTo.getMoney() + amount);

    }


    public synchronized long getBalance(String accountNum){
        long balance = 0;
        for (int i = 1; i <= accounts.size(); i++){
            if (accountNum.equals(accounts.get(i).getAccNumber())){
                balance = accounts.get(i).getMoney();
            }
        }
        return balance;
    }

    public Map<Integer, Account> getList(){
        return accounts;
    }

    public Map<String, Account> getLockedAccounts() {
      return lockedAccounts;
    }

  public void lockAccount(Account... accounts){

      for (Account account : accounts){
        lockedAccounts.put(account.getAccNumber(), account);
      }

  }

}
