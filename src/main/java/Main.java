
import java.util.Map;

public class Main {

  public static void main(String[] args) {

    Bank bank = new Bank();
    Map<Integer, Account> hashMap = bank.getList();
    final int transactionsCount = 10000;


    for (int i = 1; i < transactionsCount; i++){

      thread(new Transfer(bank, i, transactionsCount));

      if ((i % 5) == 0){

        thread(new Balance(bank, i, hashMap));

      }
    }

  }

  private static void thread(Runnable runnable){

    Thread thread = new Thread(runnable);
    thread.start();

  }

}
