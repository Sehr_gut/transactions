

public class Transfer implements Runnable {

  private final Bank bank;

  private int transfersCount;
  private int fromAccountNum;
  private int toAccountNum = (int)(100 * Math.random());

  private long amount = (long)(100 * Math.random());

  public Transfer(Bank bank, int fromAccountNum, int transfersCount){
    this.bank = bank;
    this.fromAccountNum = fromAccountNum;
    this.transfersCount = transfersCount;
  }

  public Transfer(Bank bank, int fromAccountNum, int toAccountNum, long amount){
    this.bank = bank;
    this.fromAccountNum = fromAccountNum;
    this.toAccountNum = toAccountNum;
    this.amount = amount;
  }

  @Override
  public void run() {

    for (int i = 0; i < transfersCount; i++) {
      int listLength = (bank.getList().keySet()).toArray().length;
      int fromAccountNum = (int) (Math.random() * listLength);
      int toAccountNum = (int) (Math.random() * listLength);
      long amount = (long) (Math.random() * 100);
      bank.transfer(fromAccountNum, toAccountNum, amount);
    }
  }

}

