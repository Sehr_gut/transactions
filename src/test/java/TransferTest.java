import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import junit.framework.TestCase;

public class TransferTest extends TestCase {

  private Map<Integer, Account> accounts = Collections.synchronizedMap(new HashMap<>());

  private  Bank bank = new Bank(accounts);

  @Override
  protected void setUp() throws Exception {

    accounts.put(1, new Account(1000000, "first"));
    accounts.put(2, new Account(2000000, "second"));
    accounts.put(3, new Account(3000000, "third"));
    accounts.put(4, new Account(4000000, "fouth"));
    accounts.put(5, new Account(5000000, "fith"));


  }

  public void testTheLock() throws InterruptedException{

    Transfer transfer1 = new Transfer(bank, 1, 2, 60000);
    Thread thread1 = new Thread(transfer1);
    thread1.start();


    Transfer transfer2 = new Transfer(bank, 1, 2, 60000);
    Thread thread2 = new Thread(transfer2);
    thread2.start();

    thread1.join();
    thread2.join();

    System.out.println(accounts.get(2).getMoney());

    assertEquals(2000000, accounts.get(2).getMoney());
  }

  public void testTrasfer() throws InterruptedException{
    System.out.println("До перевода: " + accounts.get(2).getMoney());

    Transfer transfer1 = new Transfer(bank, 1, 2, 40000);
    Thread thread = new Thread(transfer1);
    thread.start();
    thread.join();
    System.out.println("После перевода: " + accounts.get(2).getMoney());
    assertEquals(2040000, accounts.get(2).getMoney());
  }

  public void testSum() throws InterruptedException{
    long sum1 = 0;
    long sum2 = 0;
    for (int i = 1; i <= accounts.size(); i++){
      sum1 += accounts.get(i).getMoney();
    }
    System.out.println("Сумма до: " + sum1);

    for (int i = 0; i < 1000; i++){
      Transfer transfer = new Transfer(bank, 1, 2, 10);
      Thread thread = new Thread(transfer);
      thread.start();
    }



    for (int i = 1; i <= accounts.size(); i++){
      sum2 += accounts.get(i).getMoney();
//      System.out.println("1 " + accounts.get(i).getMoney());
    }
    System.out.println("Сумма после: " + sum2);

    assertEquals(sum1, sum2);

  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
  }
}
